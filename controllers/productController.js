const Product = require("../models/Product.js");
const Order = require("../models/Order.js")

// Add Product
module.exports.addProduct = (data) => {
		let newProduct = new Product({
			inGameName : data.inGameName,
			description : data.description,
			price : data.price
		});
		return newProduct.save().then((course, error) => {
			if (error) {
				return false;
			} else {
				return ("NFT Created");
			};
		});
};

// Get All Active Products
module.exports.getAllActiveProducts = () =>{
	return Product.find({ isActive: true }).then(result =>{
		return result;
	})
};

// Get Single Product
module.exports.getProduct = (reqParams) =>{
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
};

// Update Product
module.exports.updateProduct = ( reqParams, reqBody ) => {
	let updateProduct = {
		price: reqBody.price
	}
	return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product, err)=>{
		if (err) {
			return false;
		}else{
			return ("Updated!");
		}
	})
};

module.exports.archiveProduct = (reqParams) => {
	let updateActiveField = {
		isActive : false
	};
	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
		if (error) {
			return false;
		} else {
			return ("Archived!");
		}
	});
};
