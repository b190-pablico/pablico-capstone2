const User = require("../models/User.js");
const auth = require("../auth.js");
const bcrypt = require("bcrypt");
const Order = require("../models/Order.js")

// Register User
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10),
		wemixId: reqBody.wemixId
	})
	return newUser.save().then((user, error) =>{
		if (error) {
			return false;
		}else{
			return ("Successfully registered");
		}
	})
};

// Login User
module.exports.loginUser = ( reqBody ) => {
	return User.findOne( { email: reqBody.email } ).then(result =>{
		if(result === null){
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result) }
			}else{
				return false;
			}
		}
	} )
};

module.exports.setAsAdmin = (reqParams, reqBody) => {
	let setAsAdmin = {
		isAdmin: reqBody.isAdmin
	}

	return User.findByIdAndUpdate(reqParams.userId, setAsAdmin).then((user, err) => {
		if (err) {
			return false;
		}else{
			return true;
		}
	})
}



module.exports.getAllUsers = () =>{
	return User.find({  }).then(result =>{
		return result;
	})
};




// create order
module.exports.createOrder = (reqBody) => {
		let newOrder = new Order({
			totalAmount: reqBody.totalAmount
		});
		return newOrder.save().then((order, error) => {
			if (error) {
				return false;
			} else {
				return ("Order created.");
			};
		});
};

// get all orders
module.exports.getAllOrders = () =>{
	return Order.find({  }).then(result =>{
		return result;
	})
};