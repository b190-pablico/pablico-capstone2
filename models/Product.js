const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	inGameName: {
		type: String,
		required: [true, "Product name is required"]
	},
	description: [
		{
			class: {
				type: String,
				required: [true, "Class is required"]
			},
			level: {
				type: Number,
				required: [true, "Level is required"]
			},
			powerScore: {
				type: Number,
				required: [true, "Power Score is required"]
			}
		}
	],
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	}
});

module.exports = mongoose.model("Product", productSchema);