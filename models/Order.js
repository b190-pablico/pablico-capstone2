const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	totalAmount: {
		type: Number,
		required: [true, "Total amount is required"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	oderedBy: [
		{
			userId: {
				type: String,
				required: [true, "userId is required"]
			},
			productId: {
				type: String,
				required: [true, "productId is requried"]
			}
		}
	]
});

module.exports = mongoose.model("Order", orderSchema);