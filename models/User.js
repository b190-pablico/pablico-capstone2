const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email:{
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required"]
	},
	wemixId: {
		type: String,
		required: [true, "WemixId is required"]
	}
});

module.exports = mongoose.model("User", userSchema);