const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController.js");
const auth = require("../auth.js");

// Creating a product
router.post("/", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(false);
	} else {
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	}
});

// Get all products
router.get('/all', (req, res)=>{
	productController.getAllActiveProducts().then(resultFromController => res.send(resultFromController));
});

// Retrieve specific product
router.get('/:productId', (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Updating product
router.put('/:productId', auth.verify, (req, res) =>{
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
} );

// Archive product
router.put("/:productId/archive", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(false);
	} else {
		productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
	}
});


module.exports = router;