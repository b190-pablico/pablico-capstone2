const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");


router.post("/register", ( req, res ) => {
	userController.registerUser( req.body ).then(resultFromController => res.send(resultFromController));
} );

router.post("/login", ( req, res ) => {
	userController.loginUser( req.body ).then( resultFromController => res.send( resultFromController ) );
} );

router.put("/:userId/setAsAdmin", auth.verify, ( req, res) => {
	userController.setAsAdmin(req.params, req.body).then(resultFromController => res.send(resultFromController));
} );

router.post("/checkout", ( req, res ) => {
	userController.createOrder( req.body ).then( resultFromController => res.send( resultFromController ) );
} );


router.get('/orders', auth.verify, (req, res)=>{
	userController.getAllOrders().then(resultFromController => res.send(resultFromController));
});

router.get('/details', auth.verify, (req, res)=>{
	userController.getAllUsers().then(resultFromController => res.send(resultFromController));
});

// User details


module.exports = router;